package be.belfius.services;

import be.belfius.domain.Animal;
import be.belfius.domain.Bear;
import be.belfius.domain.Giraffe;
import be.belfius.domain.Lion;
import be.belfius.repositories.GenericCrudRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AnimalServiceImplTest {
    @Mock
    private GenericCrudRepository<Animal> animalGenericCrudRepository;

    @InjectMocks
    private AnimalServiceImpl animalMyService;

    @Before
    public void setUp() throws Exception {
        Bear bear = new Bear("TBaloe");
        Lion lion = new Lion("TSimba");
        Giraffe giraffe = new Giraffe("TLangePoot");
        when(animalGenericCrudRepository.findAll()).thenReturn(Arrays.asList(lion, bear, giraffe));
    }

    @Test
    public void findAll() {
        List<Animal> animals = animalMyService.findAll();
        Assert.assertEquals(3, animals.size());
        verify(animalGenericCrudRepository, times(1)).findAll();
    }

    @Test
    public void checkThatNameOfAnimalIsCorrect (){
        List<Animal> animals = animalMyService.findAll();
        Assert.assertTrue(animals.stream().anyMatch(animal -> animal.getName().equals("TLangePoot")));
    }


}
