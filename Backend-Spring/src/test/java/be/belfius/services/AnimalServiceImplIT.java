package be.belfius.services;

import be.belfius.BaseItTest;
import be.belfius.domain.Animal;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

import static org.hamcrest.Matchers.isOneOf;
import static org.junit.Assert.*;


public class AnimalServiceImplIT extends BaseItTest { // integration test
    @Autowired
    @Qualifier("animal")
    private GenericCrudService<Animal> myAnimalService;
    @Autowired
    private FoodDistributionService foodDistributionService;

    @Test
    public void findAll() {
        List<Animal> animals = myAnimalService.findAll();
        assertFalse(animals.isEmpty());
    }

    @Test
    public void testFindByNameAndAssertFood() {
        foodDistributionService.feedAnimals(myAnimalService.findAll());
        Animal animal = myAnimalService.findByName("blub");
        assertNotNull(animal);
        assertNotNull(animal.getFood());
        assertThat(animal.getFood().getFoodName(), isOneOf("salmon", "honey"));
    }


}
