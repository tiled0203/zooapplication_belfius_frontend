package be.belfius.restcontrollers;

import be.belfius.domain.*;
import be.belfius.domain.enums.AnimalType;
import be.belfius.dto.AnimalDto;
import be.belfius.services.AnimalServiceImpl;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/animals")
@Slf4j
@AllArgsConstructor
public class AnimalRestController {

    private AnimalServiceImpl animalService;

    //    @RequestMapping(path = "/animals", method = RequestMethod.GET)
    @GetMapping(path = "")
    public List<Animal> findAllAnimals() {
        log.info("Fetching all animals");
        return animalService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Animal findById(@PathVariable("id") int id) throws NotFoundException {
        return animalService.findById(id);
    }

    @PostMapping(path = "")
    public Animal saveAnimal(@RequestBody AnimalDto animalDto) {
        Animal animal = convertToAnimalEntity(animalDto);
        animalService.save(animal); // the save is also an update but you neet to pass the id with the dto
        // without it it will create a new animal
        return animal;
    }

    private Animal convertToAnimalEntity(AnimalDto animalDto) {
        Animal animal = null;
        switch (animalDto.getAnimalType()) {
            case BEAR:
                animal = new ModelMapper().map(animalDto, Bear.class);
//                animal = new Bear(animalDto.getName(), animalDto.getAnimalType(), animalDto.getFood(), animalDto.getFoodType());
                ((Bear) animal).setDateOfBirth(animalDto.getDateOfBirth());
                break;
            case GIRAFFE:
                animal = new ModelMapper().map(animalDto, Giraffe.class);
//                animal = new Giraffe(rs.getString("name"));
                break;
            case LION:
                animal = new ModelMapper().map(animalDto, Lion.class);
//                animal = new Lion(rs.getString("name"));
                break;
            case DOG:
                animal = new ModelMapper().map(animalDto, Dog.class);
//                animal = new Dog(rs.getString("name"));
                break;
            default:
                throw new NoSuchElementException("cannot find domain class");
        }
        return animal;
    }

    @PutMapping(path = "")
    public int updateAnimal(@RequestBody AnimalDto animalDto) {
        Animal animal = convertToAnimalEntity(animalDto);
        animalService.save(animal); // the save is also an update but you neet to pass the id with the dto
        return animal.getId();
    }

    @GetMapping(path = "/animalTypes")
    public List<AnimalType> fetchAllAnimalTypes() {
        return Arrays.asList(AnimalType.values());
    }


    @DeleteMapping("/{id}")
    public void removeById(@PathVariable("id") int id) throws NotFoundException {
        log.info("deleting animal with id {}", id);
        animalService.deleteById(id);
    }
}
