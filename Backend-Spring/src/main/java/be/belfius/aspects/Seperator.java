package be.belfius.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Seperator {

    @Pointcut("execution(* be.belfius.Executions.*(..))")
    public void addSeperator() {

    }

    @Around("addSeperator()")
    public void printSeperator(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("---------" + joinPoint.getSignature() + "---------");
        joinPoint.proceed();
        System.out.println("--------------------------------------------------");
    }

}
