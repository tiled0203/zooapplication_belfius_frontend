package be.belfius.dto;

import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import lombok.Data;

import java.time.LocalDate;

@Data
public class AnimalDto {
    private int id;
    private FoodDto food;
    private FoodType foodType;
    private AnimalType animalType;
    private String name;
    private LocalDate dateOfBirth;
}
