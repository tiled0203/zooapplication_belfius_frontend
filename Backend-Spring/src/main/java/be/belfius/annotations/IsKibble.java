package be.belfius.annotations;

import be.belfius.configurations.TimeCondition;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Primary;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Primary
@Conditional(TimeCondition.class)
public @interface IsKibble {
}
