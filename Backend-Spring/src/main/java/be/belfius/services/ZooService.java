package be.belfius.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static java.time.Instant.ofEpochMilli;

@Service
public class ZooService {
    @Autowired
    Environment env;
    private String zooOwner;
    @Value("#{T(System).currentTimeMillis()}")
    private long time;


    public ZooService(@Value("${zooOwner}") String zooOwner) {
        this.zooOwner = zooOwner;
    }

    public void printZooName() {
        System.out.println(env.getProperty("zooName"));
    }

    public void printOwner() {
        System.out.println(zooOwner);
    }

    public void printTime(){
        System.out.println(LocalDateTime.ofInstant(ofEpochMilli(time), ZoneId.of("Europe/Brussels")));
    }
}
