package be.belfius.services;


import be.belfius.domain.Animal;
import be.belfius.repositories.springdata.AnimalRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Qualifier("animal")
@Transactional
public class AnimalServiceImpl implements GenericCrudService<Animal> { // renamed MyService to GenericCrudService

    //    @Autowired // old way of dependency injection (not recommended)
    private AnimalRepository animalRepository;

    public AnimalServiceImpl( AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @Override
    public List<Animal> findAll() {
        return animalRepository.findAll();
    }

    @Override
    public void save(Animal animal) {
        animalRepository.save(animal);
    }

    @Override
    public void deleteById(int id) {
        animalRepository.deleteById(id);
    }


    @Override
    public Animal findByName(String name) {
        return animalRepository.findByName(name);
    }

    @Override
    public int count() {
        return findAll().size();
    }

    public Animal findById(int id) throws NotFoundException {
        return animalRepository.findById(id).orElseThrow(() -> new NotFoundException("Animal with id " + id + " not found"));
    }

}
