package be.belfius.services;

import be.belfius.domain.Bear;
import be.belfius.repositories.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Qualifier("bearService")
public class BearServiceImpl implements GenericCrudService<Bear> {
    private GenericCrudRepository<Bear> bearGenericCrudRepository;

    public BearServiceImpl(@Qualifier("bear") GenericCrudRepository<Bear> bearRepository) {
        this.bearGenericCrudRepository = bearRepository;
    }

    @Override
    public List<Bear> findAll() {
        return bearGenericCrudRepository.findAll();
    }

    @Override
    public void save(Bear entity) {
    }

    @Override
    public void deleteById(int id) {

    }

    @Override
    public Bear findByName(String name) {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }
}
