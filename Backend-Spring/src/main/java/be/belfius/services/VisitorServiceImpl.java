package be.belfius.services;

import be.belfius.domain.Visitor;
import be.belfius.domain.VisitorNameAddressId;
import be.belfius.domain.enums.VisitorType;
import be.belfius.exception.InvalidActionException;
import be.belfius.repositories.springdata.VisitorRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackOn = InvalidActionException.class)
public class VisitorServiceImpl {

    private VisitorRepository visitorRepository; // using spring data

    public VisitorServiceImpl(VisitorRepository visitorRepository) {
        this.visitorRepository = visitorRepository;
    }

    public List<Visitor> findAll() {
        return visitorRepository.findAll();
    }

    public Visitor findByVisitorNameAgeId(VisitorNameAddressId visitorNameAddressId) throws NotFoundException {
        Optional<Visitor> visitor = visitorRepository.findByVisitorNameAddressId(visitorNameAddressId);
        return visitor.orElseThrow(() -> new NotFoundException("visitor not found"));
    }

    public void save(Visitor visitor) {
        visitorRepository.save(visitor);// spring data
    }

    public List<Visitor> findVisitorsByType(VisitorType visitorType) {
        return visitorRepository.findVisitorsByVisitorType(visitorType);// spring data
    }

    public List<String> findDistinctTypes() {
        return visitorRepository.findDistinctVisitorTypes().stream().map(Enum::name).collect(Collectors.toList());// spring data
    }

    public int countVisitors() {
        return (int) visitorRepository.count(); // spring data
    }

    public void delete(Visitor visitor) throws InvalidActionException {
        visitorRepository.delete(visitor);
//        if(visitor.getId() == 1){
//            throw new InvalidActionException("cannot deleteById number 1 <= this is prohibited");
//        }
    }
}
