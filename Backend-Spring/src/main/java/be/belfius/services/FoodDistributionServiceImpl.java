package be.belfius.services;

import be.belfius.domain.Animal;
import be.belfius.domain.Food;
import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import be.belfius.repositories.springdata.FoodRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class FoodDistributionServiceImpl implements FoodDistributionService {

    private FoodRepository foodRepository;

    public FoodDistributionServiceImpl(FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
    }

    @Override
    public List<Food> findFoodByAnimalType(AnimalType animalType) {
        return foodRepository.findFoodByAnimalType(animalType);
    }

    @Override
    public void feedAnimals(List<Animal> animals) {
//        animals.forEach(animal -> { // streams
//            animal.setFood(foodRepository.findFoodByFoodType(animal.getClass()));
//        });

//        for (int i = 0; i < animals.size(); i++) { // normal for loop
//            animals.get(i).setFood(foodRepository.findFoodByFoodType(animals.get(i).getClass()));
//        }
//
        for (Animal animal : animals) {
            List<Food> foodForAnimalType = findFoodByAnimalType(animal.getAnimalType());
            animal.setFood(foodForAnimalType.isEmpty() ? null : foodForAnimalType.get(new Random().nextInt(foodForAnimalType.size())));
        }
    }

    @Override
    public List<Food> findAllFoods() {
        return foodRepository.findAll();
    }

    @Override
    public Food findFoodById(int id) throws NotFoundException {
        return foodRepository.findById(id).orElseThrow(() -> new NotFoundException(("Food not found")));
    }

    @Override
    public List<FoodType> findAllAvailableFoodTypesByAnimalType(AnimalType animalType) {
        return foodRepository.findAllAvailableFoodTypesByAnimalType(animalType);
    }

    @Override
    public List<FoodType> findAllAvailableFoodTypes() {
        return foodRepository.findAllAvailableFoodTypes();
    }
}
