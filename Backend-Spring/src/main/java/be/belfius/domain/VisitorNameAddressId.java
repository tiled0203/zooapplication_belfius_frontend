package be.belfius.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class VisitorNameAddressId implements Serializable {
    private String name;
    @ManyToOne
    private Address address;

    public VisitorNameAddressId() {
    }

    public VisitorNameAddressId(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
