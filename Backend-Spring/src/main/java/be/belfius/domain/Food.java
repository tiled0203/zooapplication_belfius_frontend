package be.belfius.domain;

import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import lombok.*;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Builder
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class Food extends BaseEntity{
    @Enumerated(EnumType.STRING)
    @Column(insertable = false,updatable = false) // comment this if you want to save data in the food table
    private FoodType foodType;
    private String foodName;

    @Enumerated(EnumType.STRING)
    private AnimalType animalType;

    public Food(String foodName) {
        this.foodName = foodName;
    }
}
