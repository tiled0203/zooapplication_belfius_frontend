package be.belfius.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("LION")
public class Lion extends Animal {
    public Lion(String name) {
        super(name);
    }

    // if you want to use the builder pattern in Animal class
//    protected Lion(Builder builder) {
//        super(builder);
//    }

    public String makeSound() {
        return "raaaaauw";
    }

    public Lion() {

    }
}
