package be.belfius.domain.enums;

public enum VisitorType {
    ADULT,CHILD,HANDICAP
}
