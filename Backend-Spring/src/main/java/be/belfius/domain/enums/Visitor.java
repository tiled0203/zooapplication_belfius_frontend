package be.belfius.domain.enums;

public enum Visitor {
    ADULT,CHILD,HANDICAP
}
