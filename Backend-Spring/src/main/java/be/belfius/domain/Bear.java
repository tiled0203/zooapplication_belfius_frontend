package be.belfius.domain;

import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("BEAR")
//id and all common properties will be inherited from the superclass Animal
public class Bear extends Animal {
    private LocalDate dateOfBirth;

    public Bear() {
    }

    public Bear(String name) {
        super(name);
    }

    public Bear(String name, AnimalType animalType, Food food, FoodType foodType) {
        super(name, animalType, food, foodType);
    }

    // if you want to use the builder pattern in Animal class
//    Bear(Builder builder) { // because i'm using the builder pattern
//        super(builder);
//    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


}
