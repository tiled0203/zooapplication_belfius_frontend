package be.belfius.domain;

import be.belfius.domain.enums.VisitorType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Visitor implements Serializable {

    @Enumerated(EnumType.STRING)
    private VisitorType visitorType;

    @EmbeddedId
    private VisitorNameAddressId visitorNameAddressId;
    //if you see in the ticket class then you see that there is also a property 'visitors'
    //this means that this relationship is Bidirectional and not Unidirectional
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ticket_fk")
    private Ticket ticket; // 0 or many vistors can have one ticket. (Visitors could be also be group or family)


    public Visitor() {
    }

    public Visitor(VisitorNameAddressId visitorNameAddressId, VisitorType visitorType){
        this.visitorNameAddressId = visitorNameAddressId;
        this.visitorType = visitorType;
    }

    public VisitorNameAddressId getVisitorNameAddressId() {
        return visitorNameAddressId;
    }

    public void setVisitorNameAddressId(VisitorNameAddressId visitorNameAddressId) {
        this.visitorNameAddressId = visitorNameAddressId;
    }


    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public VisitorType getVisitorType() {
        return visitorType;
    }

    public void setVisitorType(VisitorType visitorType) {
        this.visitorType = visitorType;
    }

}
