package be.belfius.repositories.jpa;

import be.belfius.domain.Bear;
import be.belfius.repositories.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Qualifier("bear")
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jpa")
public class BearRepositoryImpl implements GenericCrudRepository<Bear> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Bear entity) {
        entityManager.persist(entity);
    }

    @Override
    public List<Bear> findAll() {
        return entityManager.createQuery("select b from Bear b", Bear.class).getResultList();
    }

    @Override
    public void delete(Bear entity) {
        entityManager.remove(entity);
    }

    @Override
    public Bear findByName(String name) {
        return null;
    }
}
