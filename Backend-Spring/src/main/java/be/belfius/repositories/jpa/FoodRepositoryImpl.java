package be.belfius.repositories.jpa;

import be.belfius.domain.Animal;
import be.belfius.domain.Food;
import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import be.belfius.repositories.jdbc.FoodRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jpa")
public class FoodRepositoryImpl implements FoodRepository {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void addFoodForAnimalType(Class<? extends Animal> animalType, Food food) {

    }

    @Override
    public List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass) {
        return entityManager.createQuery("select f from Food f where f.animalType = :animalType and not f.foodType = 'KIBBLE'", Food.class)
                .setParameter("animalType", AnimalType.valueOf(animalClass.getSimpleName().toUpperCase())).getResultList();
    }

    @Override
    public List<Food> findFoodByFoodType(FoodType foodType) {
        return entityManager.createQuery("select f from Food f where f.foodType = :foodType", Food.class)
                .setParameter("foodType", foodType).getResultList();
    }
}
