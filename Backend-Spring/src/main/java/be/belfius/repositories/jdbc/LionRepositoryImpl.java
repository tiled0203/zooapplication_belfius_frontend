package be.belfius.repositories.jdbc;

import be.belfius.annotations.Predator;
import be.belfius.domain.Lion;
import be.belfius.repositories.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@Qualifier("lion")
@Predator
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class LionRepositoryImpl implements GenericCrudRepository<Lion> {
    @Override
    public void save(Lion entity) {

    }

    @Override
    public List<Lion> findAll() {
        System.out.println("LionRepositoryImpl");

        return Collections.emptyList();
    }

    @Override
    public void delete(Lion animal) {

    }

    @Override
    public Lion findByName(String name) {
        return null;
    }
}
