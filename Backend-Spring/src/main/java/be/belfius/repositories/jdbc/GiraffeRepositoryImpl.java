package be.belfius.repositories.jdbc;

import be.belfius.annotations.Pray;
import be.belfius.domain.Giraffe;
import be.belfius.repositories.GenericCrudRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@Pray
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class GiraffeRepositoryImpl implements GenericCrudRepository<Giraffe> {


    @Override
    public void save(Giraffe entity) {
    }

    @Override
    public List<Giraffe> findAll() {
        System.out.println("GiraffeRepositoryImpl");
        return Collections.emptyList();
    }

    @Override
    public void delete(Giraffe animal) {

    }

    @Override
    public Giraffe findByName(String name) {
        return null;
    }
}
