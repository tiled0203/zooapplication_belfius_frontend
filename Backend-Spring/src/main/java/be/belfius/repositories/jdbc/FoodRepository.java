package be.belfius.repositories.jdbc;

import be.belfius.domain.Animal;
import be.belfius.domain.Food;
import be.belfius.domain.enums.FoodType;

import java.util.List;

public interface FoodRepository {
    void addFoodForAnimalType(Class<? extends Animal> animalType, Food food);

    List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass);

    List<Food> findFoodByFoodType(FoodType foodType);
}
