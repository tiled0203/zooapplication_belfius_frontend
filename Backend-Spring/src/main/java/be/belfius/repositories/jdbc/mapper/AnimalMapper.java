package be.belfius.repositories.jdbc.mapper;

import be.belfius.domain.*;
import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.NoSuchElementException;

public class AnimalMapper implements RowMapper<Animal> {

    @Override
    public Animal mapRow(ResultSet rs, int rowNum) throws SQLException {
        Animal animal = null;
        switch (AnimalType.valueOf(rs.getString("animalType"))) {
            case BEAR:
                animal = new Bear(rs.getString("name"));
                break;
            case GIRAFFE:
                animal = new Giraffe(rs.getString("name"));
                break;
            case LION:
                animal = new Lion(rs.getString("name"));
                break;
            case DOG:
                animal = new Dog(rs.getString("name"));
                break;
            default:
                throw new NoSuchElementException("cannot find domain class for check rowmapper" + rs.getString("animalType"));
        }
        animal.setFoodType(FoodType.valueOf(rs.getString("foodType")));
        if (rs.getInt("food_id") != 0) {
            Food food = new Food("special Kibble");
            food.setId(rs.getInt("food_id"));
            food.setFoodName(rs.getString("foodName"));
            animal.setFood(food);
            animal.setId(rs.getInt("id"));
        }
        return animal;
    }
}
