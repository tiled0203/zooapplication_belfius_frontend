package be.belfius.repositories.jdbc;

import be.belfius.domain.Bear;
import be.belfius.repositories.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("bear")
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class BearRepositoryImpl implements GenericCrudRepository<Bear> {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(Bear animal) {

    }

    @Override
    public List<Bear> findAll() {
        return jdbcTemplate.query("select * from Bear", new BeanPropertyRowMapper<Bear>(Bear.class));
    }

    @Override
    public void delete(Bear animal) {

    }

    @Override
    public Bear findByName(String name) {
        return null;
    }
}
