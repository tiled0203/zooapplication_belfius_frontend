package be.belfius.repositories.springdata;

import be.belfius.domain.Food;
import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food, Integer> {
    List<Food> findFoodByAnimalType(AnimalType animalType);
    @Query("select distinct(f.foodType) from Food f")
    List<FoodType> findAllAvailableFoodTypes();
    @Query("select distinct(f.foodType) from Food f where f.animalType= :animalType")
    List<FoodType> findAllAvailableFoodTypesByAnimalType(AnimalType animalType);

}
