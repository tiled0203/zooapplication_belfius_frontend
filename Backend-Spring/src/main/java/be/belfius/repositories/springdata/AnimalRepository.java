package be.belfius.repositories.springdata;

import be.belfius.domain.Animal;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AnimalRepository extends PagingAndSortingRepository<Animal, Integer> {
    List<Animal> findAll();

    Animal findByName(String name);
}
