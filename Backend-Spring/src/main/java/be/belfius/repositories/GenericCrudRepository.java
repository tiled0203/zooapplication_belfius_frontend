  package be.belfius.repositories;

  import java.util.List;

  public interface GenericCrudRepository<T> { // changed the name from myRepository to GenericCrudRepository
      void save(T entity); //create
      List<T> findAll(); //read
      void delete(T entity); //deleteById
      T findByName(String name); //read
  }
