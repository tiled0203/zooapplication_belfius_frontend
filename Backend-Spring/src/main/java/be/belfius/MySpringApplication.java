package be.belfius;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication // Spring boot powerful annotation
public class MySpringApplication { //implements CommandLineRunner {
    @Autowired
    private Executions executions;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(MySpringApplication.class).profiles("dev").build().run();
//        ConfigurableApplicationContext context = SpringApplication.run(MySpringApplication.class);
//        System.out.println(((Bear) animalMyService.findEntityByName("Baloe")).getDateOfBirth());

    }
//
//    @Override
//    public void run(String... args) {
//        executions.printZooProperties();
//        executions.findAllAnimals();
//        executions.feedAllAnimals();
//        executions.findAllBears();
//        executions.saveAnimal();
//        executions.findAnimalByName();
//        executions.findAllVisitors();
//        executions.saveVisitor();
//        executions.findVisitorsByVisitorType();
//        executions.findDistinctvisitortypes();
//        executions.countVisitors();
//        executions.deleteVisitor();
////        try {
////            executions.throwException();
////        } catch (Exception e) {
////        }
//    }



}
