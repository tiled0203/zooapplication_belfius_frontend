package be.belfius.exception;

public class InvalidActionException extends Throwable {
    public InvalidActionException(String s) {
        super(s);
    }
}
