package be.belfius;

import be.belfius.domain.*;
import be.belfius.domain.enums.FoodType;
import be.belfius.domain.enums.VisitorType;
import be.belfius.exception.InvalidActionException;
import be.belfius.services.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Executions {
    private AddressService addressService;
    @Qualifier("animal")
    private GenericCrudService<Animal> animalService;
    @Qualifier("bearService")
    private GenericCrudService<Bear> bearService;
    private ZooService zooService;
    private FoodDistributionService foodDistributionService;
    private VisitorServiceImpl visitorService;


    public Executions(AddressService addressService, GenericCrudService<Animal> animalService, GenericCrudService<Bear> bearService, ZooService zooService, FoodDistributionService foodDistributionService, VisitorServiceImpl visitorService) {
        this.addressService = addressService;
        this.animalService = animalService;
        this.bearService = bearService;
        this.zooService = zooService;
        this.foodDistributionService = foodDistributionService;
        this.visitorService = visitorService;
    }

    void deleteVisitor() {
        try {
            Address address = addressService.findById(1);
            visitorService.delete(visitorService.findByVisitorNameAgeId(new VisitorNameAddressId("Tom", address)));
        } catch (InvalidActionException | NotFoundException e) {
            e.printStackTrace();
        }
    }

    void countVisitors() {
        System.out.println("count visitors: " + visitorService.countVisitors());
    }

    void findDistinctvisitortypes() {
        visitorService.findDistinctTypes().forEach(System.out::println);
    }

    void findVisitorsByVisitorType() {
        visitorService.findVisitorsByType(VisitorType.ADULT).forEach(visitor -> System.out.println(visitor.getVisitorNameAddressId().getName()));
    }

    void saveVisitor() {
        Address address = new Address("Somewhere", "5B", "Geraardsbergen");
        addressService.save(address);
        Visitor visitor = new Visitor(new VisitorNameAddressId("Tim", address), VisitorType.ADULT);
        visitorService.save(visitor);
    }

    void findAllVisitors() {
        visitorService.findAll().forEach(visitor -> System.out.println(visitor.getVisitorNameAddressId().getName()));
    }

    void findAnimalByName() {
        Animal animal = animalService.findByName("Max");
        if (animal == null) {
            System.out.println("Animal not found");
        } else {
            System.out.println(animal.getFoodType());
        }
    }

    void saveAnimal() {
        Dog dog = new Dog("Brutus");
//        Food meat = Food.builder().animalType(AnimalType.DOG).foodName("chicken").foodType(FoodType.MEAT).build();
//        dog.setFood(meat);
        dog.setFoodType(FoodType.MEAT);
        animalService.save(dog);
    }

    void findAllAnimals() {
        animalService.findAll()
                .forEach(animal -> System.out.printf("| animalType: %s | name: %s \t| food: %s \t|\n"
                        , animal.getClass().getSimpleName(), animal.getName(), animal.getFood() != null ? animal.getFood().getFoodName() : "No food"));
    }

    void findAllBears() {
        bearService.findAll().forEach(bear -> System.out.printf("name: %s dateOfBirth: %s \n", bear.getName(), bear.getDateOfBirth()));
    }

    void feedAllAnimals() {
        List<Animal> animals = animalService.findAll();
        foodDistributionService.feedAnimals(animals);
        animals.forEach(animal -> System.out.printf("| animalType: %s | name: %s \t| food: %s \t|\n", animal.getClass().getSimpleName(), animal.getName(), animal.getFood() != null ? animal.getFood().getFoodName() : "No food"));
    }

    void printZooProperties() {
        zooService.printZooName();
        zooService.printOwner();
        zooService.printTime();
    }

    public void throwException() throws Exception {
        throw new NotFoundException("helloooo ");
    }
}
