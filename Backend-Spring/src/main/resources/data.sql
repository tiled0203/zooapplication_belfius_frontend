INSERT INTO Food(id, foodName, foodType, animalType)
VALUES (1, 'honey', 'VEGGIE', 'BEAR')
     , (2, 'salmon', 'FISH', 'BEAR')
     , (3, 'pedigree 77', 'MEAT', 'DOG')
     , (4, 'beef', 'MEAT', 'LION')
     , (5, 'leaf', 'VEGGIE', 'GIRAFFE')
     , (6, 'Giraffe leaf cookie', 'KIBBLE', 'GIRAFFE')
     , (7, 'Honey cookie', 'KIBBLE', 'BEAR')
     , (8, 'Bone', 'KIBBLE', 'DOG')
     , (9, 'beef', 'MEAT', 'DOG');

INSERT INTO Animal(animalType, id, foodType, name, food_id)
VALUES ('DOG', 1, 'MEAT', 'Fluffy', 3)
     , ('GIRAFFE', 2, 'VEGGIE', 'tess', NULL)
     , ('BEAR', 3, 'FISH', 'blub', 2)
     , ('LION', 4, 'MEAT', 'ugly', 4)
     , ('LION', 5, 'MEAT', 'jaws', NULL)
     , ('DOG', 6, 'MEAT', 'Max', 9);

insert into Dog values(1);
INSERT INTO Bear(id,dateOfBirth) VALUES (3,'2000-2-2');
insert into Lion values(4);
insert into Lion values(5);
insert into Giraffe values(2);

insert into Address(city, houseNumber, street)values ('Kalmthout', '4B' , 'testStreet');

INSERT INTO Visitor(address_id, name, visitorType, ticket_fk)

VALUES (1, 'Tom', 'ADULT', NULL),
       (1, 'Jan', 'ADULT', NULL),
       (1, 'Liam', 'CHILD', NULL);

