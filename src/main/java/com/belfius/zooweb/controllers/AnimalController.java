package com.belfius.zooweb.controllers;

import com.belfius.zooweb.domain.Animal;
import com.belfius.zooweb.domain.Food;
import com.belfius.zooweb.domain.enums.AnimalType;
import com.belfius.zooweb.domain.enums.FoodType;
import com.belfius.zooweb.services.AnimalService;
import com.belfius.zooweb.services.FoodService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/animals")
public class AnimalController {

    private Logger logger = LoggerFactory.getLogger(AnimalController.class);

    private AnimalService animalService;
    private FoodService foodService;

    public AnimalController(AnimalService animalService, FoodService foodService) {
        this.animalService = animalService;
        this.foodService = foodService;
    }

    @GetMapping("")
    public ModelAndView showAnimals(ModelMap model) {
        try {
            model.put("animals", animalService.findAll());
            logger.info(String.valueOf(model.get("animals")));
            return new ModelAndView("animals", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }

    @GetMapping("/add")
    public ModelAndView showAddAnimalPage(ModelMap model) {
        try {
            logger.info("show add animal page");
            model.put("animalTypes", animalService.findAllAnimalTypes());
            model.put("foodTypes", foodService.findAllFoodTypes());
            model.put("foods", foodService.findAllFoods());
            model.addAttribute("add", true);
            return new ModelAndView("create-edit-animal", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }

    @GetMapping("/{id}/edit")
    public ModelAndView showEditAnimalPage(@PathVariable("id") int id, ModelMap model) {
        logger.info("show edit animal page");
        try {
            Animal animal = animalService.findById(id);
            model.put("animal", animal);

            List<AnimalType> allAnimalTypes = animalService.findAllAnimalTypes();
            allAnimalTypes.forEach(animalType -> animalType.setSelected(animalType == animal.getAnimalType()));
            model.put("animalTypes", allAnimalTypes);

            List<FoodType> allFoodTypes = foodService.findAllFoodTypesByAnimalType(animal.getAnimalType());
            allFoodTypes.forEach(foodType -> foodType.setSelected(foodType == animal.getFoodType()));
            model.put("foodTypes", allFoodTypes);

            List<Food> foods = foodService.findAllFoodByAnimalType(animal.getAnimalType());
            if (animal.getFood() != null) {
                foods.forEach(food -> food.setSelected(food.getId() == animal.getFood().getId()));
                model.put("foods", foods);
            } else {
                model.put("foods", foods);
            }

            model.addAttribute("add", false);
            return new ModelAndView("create-edit-animal", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }


    @GetMapping(value = {"/{id}/delete"})
    public ModelAndView showDeleteAnimalConfirmMessage(@PathVariable("id") int id, ModelMap model) {
        logger.info("showing animal delete page with id {} ", id);
        try {
            Animal animal = animalService.findById(id);
            model.put("animal", animal);
            model.put("allowDelete", true);
            return new ModelAndView("animal-detail", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }

    // --------------Actions---------
    @PostMapping("/{id}/edit")
    public ModelAndView editAnimal(@ModelAttribute("animal") @Valid Animal animal, ModelMap model) {
        try {
            logger.info("editing animal with id {}", animal.getId());

            animalService.save(animal);
            return new ModelAndView("redirect:/animals", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }

    @PostMapping("/add")
    public ModelAndView createAnimal(@ModelAttribute("animal") Animal animal, ModelMap model) {
        try {
            logger.info("creating animal");
            animalService.save(animal);
            return new ModelAndView("redirect:/animals", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }

    @PostMapping("/{id}/delete")
    public ModelAndView delete(@PathVariable("id") int id, ModelMap model) {
        logger.info("removing animal with id {} ", id);
        try {
            animalService.removeById(id);
            return new ModelAndView("redirect:/animals", model);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            model.put("error", e.getMessage());
            return new ModelAndView("error", model);
        }
    }
}
