package com.belfius.zooweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class HomeController {

    @GetMapping("/")
    public ModelAndView displayArticle(Map<String, Object> model) {
        return new ModelAndView("index", model);
    }

    @GetMapping("/test/test2")
    public ModelAndView showAnimals(Map<String, Object> model) {
        model.put("errorMessage", "test");
        return new ModelAndView("error", model);
    }

}
