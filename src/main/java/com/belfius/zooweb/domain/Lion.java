package com.belfius.zooweb.domain;

public class Lion extends Animal {

    protected Lion(Builder builder) {
        super(builder);
    }

    public String makeSound() {
        return "raaaaauw";
    }

    public Lion() {

    }
}
