package com.belfius.zooweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bear extends Animal {
    private LocalDate dateOfBirth;

    public Bear() {
    }

    Bear(Builder builder) { // because i'm using the builder pattern
        super(builder);
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


}
