package com.belfius.zooweb.domain;

import com.belfius.zooweb.domain.enums.VisitorType;

import java.io.Serializable;

public class Visitor implements Serializable {
    private VisitorType visitorType;
    private String name;
    private Address address;
    private Ticket ticket;

    public Visitor() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public VisitorType getVisitorType() {
        return visitorType;
    }

    public void setVisitorType(VisitorType visitorType) {
        this.visitorType = visitorType;
    }

}
