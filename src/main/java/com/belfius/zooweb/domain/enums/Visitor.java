package com.belfius.zooweb.domain.enums;

public enum Visitor {
    ADULT,CHILD,HANDICAP
}
