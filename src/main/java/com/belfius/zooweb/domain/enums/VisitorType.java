package com.belfius.zooweb.domain.enums;

public enum VisitorType {
    ADULT,CHILD,HANDICAP
}
