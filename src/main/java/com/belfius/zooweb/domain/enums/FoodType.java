package com.belfius.zooweb.domain.enums;

public enum FoodType {
    MEAT("Meat"), VEGGIE("Veggie"), UNKNOWN("Unknown"), FISH("Fish"), KIBBLE("Kibble");

    private boolean selected = false;
    private String name;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    FoodType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
