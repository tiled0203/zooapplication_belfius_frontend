package com.belfius.zooweb.domain.enums;

public enum AnimalType {
    BEAR("Bear"),DOG("Dog"),GIRAFFE("Giraffe"),LION("Lion");

    private String name;
    private boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    AnimalType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
