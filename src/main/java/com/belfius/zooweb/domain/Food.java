package com.belfius.zooweb.domain;

import com.belfius.zooweb.domain.enums.AnimalType;
import com.belfius.zooweb.domain.enums.FoodType;

public class Food extends BaseEntity {
    private FoodType foodType;
    private String foodName;
    private AnimalType animalType;
    private boolean selected;

    public Food() {
    }

    public Food(int id, FoodType foodType, AnimalType animalType, String foodName) {
        super.setId(id);
        this.foodType = foodType;
        this.animalType = animalType;
        this.foodName = foodName;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean getSelected() {
        return selected;
    }
}
