package com.belfius.zooweb.domain;

import com.belfius.zooweb.domain.enums.AnimalType;
import com.belfius.zooweb.domain.enums.FoodType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Animal extends BaseEntity {
    private Food food;
    private FoodType foodType;
    private AnimalType animalType;
    private String name;

    public Animal(Food food, FoodType foodType, AnimalType animalType, String name) {
        this.food = food;
        this.foodType = foodType;
        this.animalType = animalType;
        this.name = name;
    }

    Animal(Builder builder) {
        this.setId(builder.id);
        this.name = builder.name;
        this.food = builder.food;
        this.foodType = builder.foodType;
        this.animalType = builder.animalType;
    }

    protected Animal() {
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public String getName() {
        return name;
    }

    public Food getFood() {
        if (food == null || food.getId() == 0) {
            return null;
        }
        return food;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public static class Builder {
        private Logger logger = LoggerFactory.getLogger(Animal.class);
        public FoodType foodType;
        private int id;
        private String name;
        private Food food;
        private AnimalType animalType;

        public Builder(AnimalType animalType, FoodType foodType) {
            this.animalType = animalType;
            this.foodType = foodType;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withFood(Food food) {
            this.food = food;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Animal build() {
            try {
                switch (this.animalType) {
                    case BEAR:
                        return new Bear(this);
                    case LION:
                        return new Lion(this);
                    case DOG:
                        return new Dog(this);
                    case GIRAFFE:
                        return new Giraffe(this);
                    default:
                        throw new ClassNotFoundException(this.animalType + " class does not exist");
                }
            } catch (ClassNotFoundException e) {
                logger.error(this.animalType + " class does not exist", e);
            }
            return null;
        }
    }


}


