package com.belfius.zooweb.services;

import com.belfius.zooweb.domain.Food;
import com.belfius.zooweb.domain.enums.AnimalType;
import com.belfius.zooweb.domain.enums.FoodType;
import com.belfius.zooweb.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FoodService {
    private Logger logger = LoggerFactory.getLogger(AnimalService.class);

    @Value("${zoo.backend.url}foods")
    private String url;

    private RestTemplate restTemplate;
//    private List<Food> foods = new ArrayList<>();


    public FoodService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
//        Food food = new Food(1, FoodType.VEGGIE, AnimalType.BEAR, "Honey");
//        Food food2 = new Food(2, FoodType.MEAT, AnimalType.DOG, "Beef");
//        Food food3 = new Food(3, FoodType.MEAT, AnimalType.LION, "Pork");
//        Food food4 = new Food(4, FoodType.MEAT, AnimalType.DOG, "Bone");
//        Food food5 = new Food(5, FoodType.FISH, AnimalType.BEAR, "Salmon");
//        Food food6 = new Food(6, FoodType.VEGGIE, AnimalType.GIRAFFE, "Leaf");
//        foods = new ArrayList<>(Arrays.asList(food, food2, food3, food4, food5, food6));
    }

    public List<FoodType> findAllFoodTypes() throws NotFoundException {
        ResponseEntity<FoodType[]> responseEntity = restTemplate.getForEntity(url + "/foodTypes", FoodType[].class);
        if (!responseEntity.hasBody() | responseEntity.getBody() == null) {
            logger.error("Cannot find foodtypes");
            throw new NotFoundException("Cannot find foodtypes ");
        }
        return new ArrayList<FoodType>(Arrays.asList(responseEntity.getBody()));
    }

    public Food findFoodById(int id) throws NotFoundException {
        ResponseEntity<Food> responseEntity = restTemplate.getForEntity(url + "/" + id, Food.class);
        if (!responseEntity.hasBody() | responseEntity.getBody() == null) {
            logger.error("Cannot find food by id {}", id);
            throw new NotFoundException("Food not found");
        }
        return responseEntity.getBody();
//        return foods.stream().filter(foodType -> foodType.getId() == id).findFirst().orElseThrow(() -> new NotFoundException("Food not found"));
    }

    public List<Food> findAllFoodByAnimalType(AnimalType animalType) throws NotFoundException {
        ResponseEntity<Food[]> responseEntity = restTemplate.getForEntity(url + "?animalType=" + animalType, Food[].class);
        if (!responseEntity.hasBody() | responseEntity.getBody() == null) {
            logger.error("Cannot find food by animaltype {}", animalType);
            throw new NotFoundException("Cannot find food by animaltype " + animalType);
        }
        return new ArrayList<Food>(Arrays.asList(responseEntity.getBody()));
    }

    public List<Food> findAllFoods() throws NotFoundException {
        ResponseEntity<Food[]> responseEntity = restTemplate.getForEntity(url, Food[].class);
        if (!responseEntity.hasBody() | responseEntity.getBody() == null) {
            logger.error("Cannot find foods");
            throw new NotFoundException("Cannot find foods ");
        }
        return Arrays.asList(responseEntity.getBody());
    }

    public List<FoodType> findAllFoodTypesByAnimalType(AnimalType animalType) throws NotFoundException {
        ResponseEntity<FoodType[]> responseEntity = restTemplate.getForEntity(url + "/foodTypes?animalType=" + animalType, FoodType[].class);
        if (!responseEntity.hasBody() | responseEntity.getBody() == null) {
            logger.error("Cannot find foodtypes by animaltype {}", animalType);
            throw new NotFoundException("Cannot find food by animaltype " + animalType);
        }
        return new ArrayList<FoodType>(Arrays.asList(responseEntity.getBody()));
    }
}
