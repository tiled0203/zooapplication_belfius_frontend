package com.belfius.zooweb.services;

import com.belfius.zooweb.domain.Animal;
import com.belfius.zooweb.domain.enums.AnimalType;
import com.belfius.zooweb.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class AnimalService {
    private Logger logger = LoggerFactory.getLogger(AnimalService.class);
    private RestTemplate restTemplate;


    @Value("${zoo.backend.url}animals")
    private String url;


    public AnimalService(RestTemplate restTemplate, FoodService foodService) {
        this.restTemplate = restTemplate;
    }

    public List<Animal> findAll() {
        ResponseEntity<Animal[]> responseEntity = restTemplate.getForEntity(url, Animal[].class);
        if (!responseEntity.hasBody()) {
            logger.error("Cannot find animals");
            return Collections.emptyList();
        }
        return Arrays.asList(responseEntity.getBody());
    }

    public void save(Animal animal) throws NotFoundException {
        ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<Animal>(animal), Integer.class);
        logger.info("animal with id {} Edited", responseEntity.getBody());
    }

    public void removeById(int id) throws NotFoundException {
        logger.info("deleting animal with id {}", id);
        restTemplate.delete(url + "/" + id);
    }

    public Animal findById(int id) throws NotFoundException {
        ResponseEntity<Animal> responseEntity = restTemplate.getForEntity(url + "/" + id, Animal.class);
        logger.info("animal with id {} found", responseEntity.getBody());
        return responseEntity.getBody();
    }

    public List<AnimalType> findAllAnimalTypes() throws NotFoundException {
        ResponseEntity<AnimalType[]> responseEntity = restTemplate.getForEntity(url + "/animalTypes", AnimalType[].class);
        logger.info("Finding all animal types {}", responseEntity.getBody());
        if (!responseEntity.hasBody()) {
            logger.error("Cannot find animal types");
            throw new NotFoundException("Cannot find animal types");
        }
        return Arrays.asList(responseEntity.getBody());
    }

}
