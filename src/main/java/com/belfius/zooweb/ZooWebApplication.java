package com.belfius.zooweb;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.io.IOException;

@SpringBootApplication
public class ZooWebApplication {

    public static void main(String[] args) throws IOException {
        new SpringApplicationBuilder(ZooWebApplication.class).build().run();
        openHomePage();
    }

    private static void openHomePage() throws IOException {
        Runtime rt = Runtime.getRuntime();
        rt.exec("rundll32 url.dll,FileProtocolHandler " + "http://localhost:8081");
    }
}
