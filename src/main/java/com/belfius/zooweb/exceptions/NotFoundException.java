package com.belfius.zooweb.exceptions;

public class NotFoundException extends Exception  {
    public NotFoundException(String s) {
        super(s);
    }
}
