package com.belfius.zooweb.formatters;

import com.belfius.zooweb.domain.Food;
import com.belfius.zooweb.exceptions.NotFoundException;
import com.belfius.zooweb.services.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class FoodFormatter implements Formatter<Food> {
    @Autowired
    private FoodService foodService;


    @Override
    public Food parse(String id, Locale locale) throws ParseException {
        if (id.equals("null")) {
            return new Food();
        }
        try {
            return foodService.findFoodById(Integer.parseInt(id));
        } catch (NotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String print(Food food, Locale locale) {
        return food.getFoodName();
    }
}
